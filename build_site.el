(require 'ox-publish)
(setq org-publish-project-alist
      (list (list "my-website"
		  :recurse t
		  :base-directory "./content"
		  :publishing-directory "./public"
		  :with-author nil
		  :with-creator t
		  :with-toc t
		  :section-numbers nil
		  :time-stamp-file nil
		  :publishing-function 'org-html-publish-to-html)))
(setq org-html-validation-link nil)
(org-publish-all t)
(message "Build complete!")
